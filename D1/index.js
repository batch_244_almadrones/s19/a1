if (0 == false) {
  console.log("False");
}

let ternaryResult = 1 < 18 ? "A" : "B";
console.log(ternaryResult);

let x = parseInt(prompt("Enter a number"));

try {
  let y = 100 ** x;
  console.log(x);
} catch {
  console.log(err.message);
} finally {
  x = 2;

  console.log(x);
}
